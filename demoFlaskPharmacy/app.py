from flask import Flask, request
import boto3
from flask_cors import CORS, cross_origin

dynamodb = boto3.resource("dynamodb", endpoint_url="http://localhost:8000")

app = Flask(__name__)
CORS(app)

@app.route("/")
def index():
    return {"message":"hey, There"}

@app.route("/data")
def get():
    table = dynamodb.Table("DemoPharmaciesList")
    scan_response = table.scan(TableName="DemoPharmaciesList")
    items = scan_response["Items"]
    items_dict= {}
    for i in range(len(items)):
        items_dict[f"a{i}"]=items[i]
    return items_dict

@app.route("/post", methods=["GET", "POST"])
def post_pharmacy():
    data = request.get_json(force=True)
    name = data["name"]
    email = data["email"]
    contact = data["contact"]
    address = data["address"]
    table = dynamodb.Table("DemoPharmaciesList")
    record = { "name":name, "email":email, "contact":contact, "address":address }
    table.put_item(Item = record)
    return { "message":"One Record Added" }

@app.route("/getrecord", methods = ["GET", "POST"])
def get_record():
    data = request.get_json(force=True)
    name = data["name"]
    table = dynamodb.Table("DemoPharmaciesList")
    key = { "name": name }
    response = table.get_item(Key = key)
    item = response["Item"]
    name = item["name"]
    email = item["email"]
    contact = item["contact"]
    address = item["address"]
    return { "name": name, "email": email, "address":address, "contact":contact }

if __name__ == "__main__":
    app.run(debug=True)